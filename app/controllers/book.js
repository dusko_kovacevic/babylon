/**
*
*/
angular.module('app.controllers').controller('BookCtrl', ['$scope', 'Members', 'Doctors', 'Appointments',
	'$mdDialog',
	function ($scope, Members, Doctors, Appointments, $mdDialog) {
		
		// Get members
		Members.get().then(function(value) {
			$scope.members = value;

			// Filter by current user
			var currentUser = $scope.members.filter(function(member) {
				return member.currentUser;
			});

			// As filter returns an array return the first item in the list
			$scope.selectedMember = currentUser[0];
		});
		
		// Get healthcare professional types
		Doctors.getHealthcareProfessionalTypes().then(function(types) {
			$scope.types = types;

			// Assign the first in the list as selected type
			$scope.selectedType = types[0];
		});

		// Get a default GP that has the earliest availability
		Doctors.getDefault('GP').then(function(doctor) {
			$scope.doctor = doctor;

			// Assume the 1st one is the latest time slot
			$scope.selectedTime = doctor.next[0];
		});

		/**
		* Select member
		*
		* @method SelectMember
		* @param {Number} id - ID of a member to select
		*/
		$scope.SelectMember = function(id) {
			// Check if we have selected someone else
			if (id === undefined) {
				$scope.selectedMember = null;
			} else {
				for (var i = $scope.members.length - 1; i >= 0; i--) {
					if ($scope.members[i].id === id) {
						$scope.selectedMember = $scope.members[i];
						break;
					}
				}
			}
		};

		/**
		* Select type
		*
		* @method SelectType
		* @param {Number} id - ID of a type to select
		*/
		$scope.SelectType = function(id) {
			for (var i = $scope.types.length - 1; i >= 0; i--) {
				if ($scope.types[i].id === id) {
					$scope.selectedType = $scope.types[i];
					break;
				}
			}

			// Find the default doctor for a type
			Doctors.getDefault($scope.selectedType.type).then(function(doctor) {
				$scope.doctor = doctor;

				// Assume the 1st one is the latest time slot
				$scope.selectedTime = doctor.next[0];
			});
		};

		/**
		* Select a different time
		*
		* @method SelectTime
		*/
		$scope.SelectDoctor = function(ev) {

			Doctors.getDoctorList($scope.selectedType.type).then(function(doctors) {

				$mdDialog.show({
					controller: DoctorCtrl(doctors), // Pass available doctors
					templateUrl: 'partials/doctor.html',
					parent: angular.element(document.body),
					targetEvent: ev,
					clickOutsideToClose:true,
					fullscreen: true
				}).then(function(doctor) {
					$scope.doctor = doctor;
					// Default to 1st available slot
					$scope.selectedTime = doctor.next[0];
				}, function() {
					// Do nothing
				});

			});

		};

		/**
		* Select a different time
		*
		* @method SelectTime
		*/
		$scope.SelectTime = function(ev) {
			$mdDialog.show({
				controller: TimeCtrl($scope.doctor.next), // Pass available appointments
				templateUrl: 'partials/time.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose:true,
				fullscreen: true
			}).then(function(time) {
				$scope.selectedTime = time;
			}, function() {
				// Do nothing
			});
		};

		/**
		* Book appointment
		*
		* @method Book
		*/
		$scope.Book = function() {
			Appointments.book($scope.selectedMember.id,
				$scope.doctor.id,
				$scope.selectedTime);
		};

	}
]);

/**
* Doctor dialog controller
*
* @method DoctorCtrl
* @param {Array} doctors - List of doctors
*/
function DoctorCtrl(doctors) {
	// Use closure to pass doctors
	return function($scope, $mdDialog) {
		$scope.doctors = doctors;
		$scope.SelectDoctor = function(doctor) {
			$mdDialog.hide(doctor);
		};
		$scope.Cancel = function() {
			$mdDialog.cancel();
		};
	};
}

/**
* Time dialog controller
*
* @method TimeCtrl
* @param {Array} appointments - Available appointments
*/
function TimeCtrl(appointments) {
	// Use closure to pass appointments
	return function($scope, $mdDialog) {
		$scope.appointments = appointments;
		$scope.SelectTime = function(time) {
			$mdDialog.hide(time);
		};
		$scope.Cancel = function() {
			$mdDialog.cancel();
		};
	};
}