/**
*
*/
var angular = require('angular');

require('angular-aria');
require('angular-animate');
require('angular-material');
require('angular-route');

angular.module('app.services', []);

// Include app service modules
require('./services/members');
require('./services/doctors');
require('./services/appointments');

angular.module('app.controllers', ['app.services', 'ngMaterial']);

// Include all controller modules
require('./controllers/home');
require('./controllers/book');

// Define App and Controller modules
var app = angular.module('app', ['ngRoute', 'app.controllers']);

// Configure route provider
app.config(['$routeProvider', 
	function($routeProvider) {
		$routeProvider.
			when('/home', {
				templateUrl: 'partials/home.html',
				controller: 'HomeCtrl'
			}).
			when('/book', {
				templateUrl: 'partials/book.html',
				controller: 'BookCtrl'
			}).
			otherwise({
				redirectTo: '/home'
			});
	}
]);