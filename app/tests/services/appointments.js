describe("Appointments service tests", function() {

	beforeEach(module('app.services'));

	var Appointments;

	beforeEach(inject(function (_Appointments_) {
		Appointments = _Appointments_;
	}));

	it("should be able to get a list of available appointments", function(done) {
		Appointments.get().then(function(appointments) {
			expect(appointments).not.toBeNull();
			done();
		});
	});

});