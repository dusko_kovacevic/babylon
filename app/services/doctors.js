/**
* Mock data
*
* @private
*/
var types = [{
		id: 0,
		type: 'GP'
	}, {
		id: 1,
		type: 'Nurse'
	}, {
		id: 2,
		type: 'Therapist'
	}, {
		id: 3,
		type: 'Specialist'
	}];

var mockData = [{
			id: 0,
			type: 'GP',
			name: 'Mr. Jeffrey Braithwaite',
			next: ['2016-05-30T14:00', '2016-05-30T15:00', '2016-05-30T16:00']
		}, {
			id: 1,
			type: 'GP',
			name: 'Mr.Rifky Guirgis',
			next: ['2016-05-31T10:00']
		}, {
			id: 2,
			type: 'GP',
			name: 'Mr.Christopher Guyer',
			next: ['2016-05-29T12:30', '2016-05-29T14:30']
		}, {
			id: 3,
			type: 'GP',
			name: 'Prof. Michael Cummings',
			next: ['2016-05-30T16:00']
		}, {
			id: 4,
			type: 'GP',
			name: 'Dr. Partha Kar',
			next: ['2016-05-31T10:30', '2016-05-31T12:00']
		}, {
			id: 5,
			type: 'GP',
			name: 'Dr. Steve Hurel',
			next: ['2016-05-30T11:00', '2016-05-31T10:00']
		}, {
			id: 6,
			type: 'GP',
			name: 'Dr. Ken Layiz',
			next: ['2016-05-31T14:30']
		}, {
			id: 7,
			type: 'Nurse',
			name: 'Title. Name Surname',
			next: ['2016-05-31T14:30']
		}, {
			id: 8,
			type: 'Therapist',
			name: 'Title. Name Surname',
			next: ['2016-05-31T14:30']
		}, {
			id: 9,
			type: 'Specialist',
			name: 'Title. Name Surname',
			next: ['2016-05-31T14:30']
		}];

/**
* Doctors service
*
* @module Doctors
*/
angular.module('app.services').factory('Doctors', [ '$q', //'$http',
	function($q) {
		return {
			getHealthcareProfessionalTypes: function() {
				// Simulating $http.get('/api/doctors/types')
				return $q(function(resolve) {
					resolve(types);
				});
			},
			/**
			* Get a default healthcare professional by earliest appointment
			*
			* @method getDefault
			* @param {String} type - Type of healthcare professional
			* @returns {Object} - Next available healthcare professional
			*/
			getDefault: function(type) {
				// Simulating $http.get('/api/doctors?default&type=' + type)

				var matchByType = mockData.filter(function(doctor) {
					return doctor.type === type;
				});

				// Loop and find the earliest appointment
				var lowest = null;
				var tmp;
				for (var i = matchByType.length - 1; i >= 0; i--) {
					
					// Assume next available appointments are sorted
					tmp = matchByType[i];
					var isEarlier = true;

					if (lowest == null) {
						lowest = tmp;
					} else if (new Date(tmp.next[0]) < new Date(lowest.next[0])) {
						lowest = tmp;
					}

				}

				return $q(function(resolve) {
					resolve(lowest);
				});
			},
			/**
			* Get a list of of healthcare professional by type
			*
			* @method getDoctorList
			* @param {String} type - Type of healthcare professional
			* @returns {Array} - List of healthcare professional
			*/
			getDoctorList: function(type) {
				// Simulating $http.get('/api/doctors?type=' + type)

				var matchByType = mockData.filter(function(doctor) {
					return doctor.type === type;
				});

				return $q(function(resolve) {
					resolve(matchByType);
				});
			},
			/**
			* Get a list of next available appointments
			*
			* @method getAppointmentList
			* @param {Number} id - Doctor ID
			* @returns {Array} - List of next available appointments
			*/
			getAppointmentList: function(id) {
				// Simulating $http.get('/api/doctors/' + id + '/appointments')

				var matchById = mockData.filter(function(id) {
					return doctor.id === id;
				});

				return $q(function(resolve) {
					resolve(matchById.next);
				});
			}
		};
	}
]);