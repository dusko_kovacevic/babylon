/**
* Mock data
*
* @private
*/
var mockData = [{
		id: 0,
		currentUser: true,
		name: 'John',
		avatar: ''
	}, {
		id: 1,
		currentUser: false,
		name: 'Sarah',
		avatar: ''
	}];

/**
* Members service
*
* @module app.services
*/
angular.module('app.services').factory('Members', ['$q', //'$http',
	/**
	* Member service
	*
	* @class Members
	*/
	function($q) {
		return {
			/**
			* Get a list of members linked to the logged in user
			*
			* @method get
			* @returns {Array} List of members linked to the logged in user
			*/
			get: function() {
				// Simulating $http.get('/api/members')
				return $q(function(resolve) {
					resolve(mockData);
				});
			}
		};
	}
]);