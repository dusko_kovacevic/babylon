/**
* Mock data
*
* @private
*/
var mockData = [{
		id: 0,
		member: 0,
		doctor: 0,
		time: '2016-05-30T14:00'
	}, {
		id: 1,
		member: 1,
		doctor: 1,
		time: '2016-05-31T10:00'
	}];

/**
* Appointments service
*
* @module app.services
*/
angular.module('app.services').factory('Appointments', ['$q', //'$http',
	/**
	* Appointment service
	*
	* @class Appointments
	*/
	function($q) {
		return {
			/**
			* Get all appointments
			*
			* @method get
			* @returns {Array} List of appointments
			*/
			get: function() {
				// Simulating $http.get('/api/appointments')
				return $q(function(resolve) {
					resolve(mockData);
				});
			},
			/**
			* Book an appointment
			*
			* @method book
			* @param {Number} member - Member ID
			* @param {Number} doctor - Doctor ID
			* @param {String} time - Time
			*/
			book: function(member, doctor, time) {
				// Simulating $http.post('/api/appointments')
				mockData.push({
					id: mockData.length,
					member: member,
					doctor: doctor,
					time: time
				});

				// We will ignore removing the available time slot from the doctors mock data

				return $q(function(resolve) {
					resolve();
				});
			}
		};
	}
]);