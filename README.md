# README #

Here are the steps are necessary to get the application up and running.

* npm install
* webpack
* npm start
* http://localhost:8080/app/

### Tools used ###

* Webpack - for module loading
* Angular - as a MVVM framework
* ngMaterial - for the UI

### Notes ###

* I have chosen to use unfamiliar technologies such as Webpack and ngMaterial, which had a consequence of not using TDD approach to coding
* Getting Jasmine tests to run via Karma, using Webpack was a challenge and therefore I have not completed the testing part of the project - Given a luxury of time I am sure this could be resolved.
* The ngMaterial tabs don't work well with a ngRoute and therefore a simple menu was added. UI was compromised to save time as ngMaterial has exceptionally poor documentation especially the layout which has only one useful example.
* Due to a learning curve it was a challenge to deliver the code, however the project is a working example which I am very happy with